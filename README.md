# QRCode scanner

## Init 

- Créer une google sheet
- Y ajouter un script comme [celui-ci](./sheet-script.js)
- Publier le script en type Web App
- Mettre l'url de l'API dans le code de l'appli react, dans `src/sender.ts`

## Déploiement

Dans un projet GCP:
- Activer AppEngine, Compute API, AppEngine Admin API
- Créer un compte de service avec les droits de deploy sur AppEngine et management du compute storage
- Mettre la clé dans une variable gitlab DEPLOY_KEY_FILE de type fichier 
