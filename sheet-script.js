
function doGet(e){
  var ss= SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1n-IYse3BXE7EIUxTBn97Q3DwsaYYg0GSZSkG4NBJAdE/edit#gid=0")
  var sheet=ss.getSheetByName("participants")
  return insert(e,sheet);
}

function doPost(e){
  
  var ss= SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1n-IYse3BXE7EIUxTBn97Q3DwsaYYg0GSZSkG4NBJAdE/edit#gid=0")
  var sheet=ss.getSheetByName("participants")
  return insert(e,sheet);
  
}

function insert(e,sheet) {
  
  var scannedData= e.parameter;
  
  var nom = scannedData.name;
  var societe = scannedData.company;
  var email = scannedData.email;
  
  sheet.appendRow([nom, email, societe]);
  
}
