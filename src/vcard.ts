const mappingVcard = {
  org: "company",
  n: "name",
  fn: "fullName",
  email: "email",
};
const regexpMapping: { [key: string]: RegExp } = {};
Object.keys(mappingVcard).forEach(
  (key) => (regexpMapping[key] = new RegExp(`^${key}:(?<value>.+)$`, "i"))
);

export type User = {
  name: string;
  company?: string;
  email: string;
  fullName?: string;
};

export function parseVcard(input: string): User {
  const data = {} as any;

  input.split(/\r\n|\r|\n/).forEach((line) => {
    console.log(line);
    Object.keys(mappingVcard).forEach((key) => {
      const match = regexpMapping[key].exec(line);
      if (match) {
        const value = match.groups?.value?.replaceAll(";", " ");
        data[mappingVcard[key]] = value;
      }
    });
  });

  return data;
}
