import { Modal } from "@material-ui/core";
import React from "react";
import QrReader from "react-qr-reader";
import { useStyles } from ".";
import { sendData } from "./sender";
import { parseVcard, User } from "./vcard";

const App: React.FC = () => {
  const [data, setData] = React.useState<User | null>(null);

  const handleScan = React.useCallback((res) => {
    if (res != null) {
      const vcard = parseVcard(res);
      setData(data);
      sendData(vcard);
    }
  }, []);

  const handleError = React.useCallback((error) => {
    console.error(error);
    window.alert("Erreur de lecture");
  }, []);

  const modalClasses = useStyles();

  return (
    <div className="App">
      <QrReader
        delay={300}
        onError={handleError}
        onScan={handleScan}
        style={{ width: "100%" }}
      />
      <Modal
        open={data != null}
        onClose={() => setData(null)}
        className={modalClasses.modal}
        onClick={() => setData(null)}
      >
        <div className={modalClasses.paper}>
          <h1>{data?.name}</h1>
          <br />
          <h2>{data?.company}</h2>
          <h2>{data?.email}</h2>
        </div>
      </Modal>
    </div>
  );
};

export default App;
